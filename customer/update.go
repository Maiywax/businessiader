package customer

import (
	"net/http"

	"github.com/jinzhu/gorm"
)

func UpdateCustomer(w http.ResponseWriter, r *http.Request) {
	db := r.Context().Value("database").(*gorm.DB)
	customer := CustomersUpdate{}
	firstName := r.FormValue("FirstName")
	id := r.FormValue("Id")
	//address := r.FormValue("Address")
	primaryPhone := r.FormValue("PrimaryPhone")
	surname := r.FormValue("Surname")
	lastName := r.FormValue("Surname")
	alternatePhone := r.FormValue("AlternatePhone")
	IdentificationNo := r.FormValue("Identification")

	err := db.Model(&customer).Where("id like ?", id).Updates(map[string]interface{}{"first_name": firstName, "last_name": lastName, "surname": surname, "primary_phone": primaryPhone, "alternate_phone": alternatePhone, "identification_no": IdentificationNo})
	if err != nil {
		return
	}
	//post data to metering
}
