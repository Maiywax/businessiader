package data

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/muhoro/log"
)

var DB *gorm.DB

func Connect(connstr, dialect string) {
	db, err := gorm.Open(dialect, connstr)
	if err != nil {
		log.Fatal(err.Error(), nil)
	}
	DB = db
}
